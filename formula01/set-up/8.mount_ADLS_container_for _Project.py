# Databricks notebook source


# COMMAND ----------

# MAGIC %md
# MAGIC #### accesss azure data lake using service principal
# MAGIC 1. register azure AD application/ service principal
# MAGIC 2. generate a secret/password for the application
# MAGIC 3. set spark config with app/client id directory tenant & secret
# MAGIC 4. assign role storage blob data contributor to the data lake

# COMMAND ----------

# MAGIC %md
# MAGIC #### Mount_azure_datalake_container_for_Project

# COMMAND ----------

def mount_adls(storage_account_name,container_name):
    # Get secret from key vault
    client_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-client-secrets')
    tenant_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-tenant-secrets')
    client_secret = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-secret')

    #Set spark configuration
    configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}
    
    if any(mount.mountPoint == f"/mnt/{storage_account_name}/{container_name}" for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(f"/mnt/{storage_account_name}/{container_name}")

    #Mount the storage account container
    dbutils.fs.mount(
    source = f"abfss://{container_name}@{storage_account_name}.dfs.core.windows.net/",
    mount_point = f"/mnt/{storage_account_name}/{container_name}",
    extra_configs = configs)

    display(dbutils.fs.mounts())

# COMMAND ----------

# MAGIC %md
# MAGIC #### Mount raw Container

# COMMAND ----------

mount_adls('formula082dl','raw')

# COMMAND ----------

# MAGIC %md
# MAGIC
# MAGIC client_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-client-secrets')
# MAGIC tenant_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-tenant-secrets')
# MAGIC client_secret = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-secret')

# COMMAND ----------

# MAGIC %md
# MAGIC #### Mount processed container

# COMMAND ----------

mount_adls('formula082dl','processed')

# COMMAND ----------

# MAGIC %md
# MAGIC #### Mount presentation container

# COMMAND ----------

mount_adls('formula082dl','presentation')

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}


# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://demo@formula082dl.dfs.core.windows.net/",
  mount_point = "/mnt/formula082dl/demo",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/formula082dl/demo"))

# COMMAND ----------

display(spark.read.csv("/mnt/formula082dl/demo/002 circuits.csv"))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

#dbutils.fs.unmount('/mnt/formula082dl/demo')

# COMMAND ----------


