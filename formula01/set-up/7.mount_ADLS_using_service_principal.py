# Databricks notebook source


# COMMAND ----------

# MAGIC %md
# MAGIC #### accesss azure data lake using service principal
# MAGIC 1. register azure AD application/ service principal
# MAGIC 2. generate a secret/password for the application
# MAGIC 3. set spark config with app/client id directory tenant & secret
# MAGIC 4. assign role storage blob data contributor to the data lake

# COMMAND ----------

client_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-client-secrets')
tenant_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-tenant-secrets')
client_secret = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-secret')

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}


# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://demo@formula082dl.dfs.core.windows.net/",
  mount_point = "/mnt/formula082dl/demo",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/formula082dl/demo"))

# COMMAND ----------

display(spark.read.csv("/mnt/formula082dl/demo/002 circuits.csv"))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

#dbutils.fs.unmount('/mnt/formula082dl/demo')

# COMMAND ----------


