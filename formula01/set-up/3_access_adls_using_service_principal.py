# Databricks notebook source


# COMMAND ----------

# MAGIC %md
# MAGIC #### accesss azure data lake using service principal
# MAGIC 1. register azure AD application/ service principal
# MAGIC 2. generate a secret/password for the application
# MAGIC 3. set spark config with app/client id directory tenant & secret
# MAGIC 4. assign role storage blob data contributor to the data lake

# COMMAND ----------

client_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-client-secrets')
tenant_id = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-tenant-secrets')
client_secret = dbutils.secrets.get(scope = 'secretScope082',key = 'formula1-app-secret')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula082dl.dfs.core.windows.net", "OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.formula082dl.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.formula082dl.dfs.core.windows.net", client_id)
spark.conf.set("fs.azure.account.oauth2.client.secret.formula082dl.dfs.core.windows.net", client_secret)
spark.conf.set("fs.azure.account.oauth2.client.endpoint.formula082dl.dfs.core.windows.net", f"https://login.microsoftonline.com/{tenant_id}/oauth2/token")

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula082dl.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula082dl.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


