# Databricks notebook source
# MAGIC %md
# MAGIC #### accesss azure data lake using access keys
# MAGIC 1. set the spark config fs.azure.account key
# MAGIC 2. list files from demo container
# MAGIC 3. read data from circuits.csv file

# COMMAND ----------

keyvault_secret_key = dbutils.secrets.get(scope = 'secretScope082',key = 'keyvault-secret-key')

# COMMAND ----------

spark.conf.set("fs.azure.account.key.formula082dl.dfs.core.windows.net",
               keyvault_secret_key)  
               

# COMMAND ----------



# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula082dl.dfs.core.windows.net"))

# COMMAND ----------

display(dbutils.fs.ls('/'))

# COMMAND ----------


