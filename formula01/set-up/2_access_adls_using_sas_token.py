# Databricks notebook source
# MAGIC %md
# MAGIC #### accesss azure data lake using sas token
# MAGIC 1. set the spark config for SAS Token
# MAGIC 2. list files from demo container
# MAGIC 3. read data from circuits.csv file

# COMMAND ----------

sas_token_secret = dbutils.secrets.get(scope = 'secretScope082',key = 'sas-token-secret')
print(sas_token_secret)

# COMMAND ----------

# spark.conf.set("fs.azure.account.auth.type.formula082dl.dfs.core.windows.net", "SAS")
# spark.conf.set("fs.azure.sas.token.provider.type.formula082dl.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
# spark.conf.set("fs.azure.sas.fixed.token.formula082dl.dfs.core.windows.net","sp=r&st=2024-04-14T11:45:27Z&se=2024-04-14T19:45:27Z&spr=https&sv=2022-11-02&sr=c&sig=jLp6DwhBh9VIPdqbP2ggI4u5LjquRgha2nS%2BSTE6d28%3D")

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula082dl.dfs.core.windows.net", "SAS")
spark.conf.set("fs.azure.sas.token.provider.type.formula082dl.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.formula082dl.dfs.core.windows.net", dbutils.secrets.get(scope="secretScope082", key="sas-token-secret"))

# COMMAND ----------



# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula082dl.dfs.core.windows.net"))

# COMMAND ----------

 display(spark.read.csv("abfss://demo@formula082dl.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------

display(dbutils.fs.ls('/'))

# COMMAND ----------


