# Databricks notebook source
# MAGIC %md
# MAGIC ####Ingest lap_times folder

# COMMAND ----------

# MAGIC %md
# MAGIC #####Step1- Read the CSV file using the spark dataframe reader API

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType, DateType

# COMMAND ----------

lap_times_schema = StructType(fields=[StructField("raceId", IntegerType(), False),
                                   StructField("driverId", IntegerType(), True),
                                   StructField("stop", IntegerType(), True),
                                   StructField("lap", StringType(), True),
                                   StructField("time", IntegerType(), True),
                                   StructField("duration", StringType(), True),
                                   StructField("milliseconds", IntegerType(), True)
                                   
                                   ])

# COMMAND ----------

lap_times_df = spark.read.schema(lap_times_schema).csv("/mnt/formula082dl/raw/lap_times")

# COMMAND ----------

display(lap_times_df)

# COMMAND ----------

lap_times_df.count()

# COMMAND ----------

# MAGIC %md
# MAGIC #####Step2- Rename columns and add new columns
# MAGIC 1. Rename driverid and raceld
# MAGIC 2. Add ingestion_date with current timestamps

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

# COMMAND ----------

final_df = lap_times_df.withColumnRenamed("driverId", "driver_id").withColumnRenamed("raceId", "race_id").withColumn("ingestion_date", current_timestamp())

# COMMAND ----------

# MAGIC %md
# MAGIC #####Step3- Write to output to processed container in parquet format

# COMMAND ----------

final_df.write.mode("overwrite").parquet("/mnt/formula082dl/processed/lap_times")

# COMMAND ----------

display(spark.read.parquet("/mnt/formula082dl/processed/lap_times"))
