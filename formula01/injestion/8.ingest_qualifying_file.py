# Databricks notebook source
# MAGIC %md
# MAGIC #### Ingest qualifying json files

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType, DateType

# COMMAND ----------

qualifying_schema = StructType(fields=[StructField("qualifyId", IntegerType(), False),
                                   StructField("raceId", IntegerType(), True),
                                   StructField("driverId", IntegerType(), True),
                                   StructField("constructorId", IntegerType(), True),
                                   StructField("number", IntegerType(), True),
                                   StructField("position", IntegerType(), True),
                                   StructField("q1", StringType(), True),
                                   StructField("q2", StringType(), True),
                                   StructField("q3", StringType(), True)
                                   ])

# COMMAND ----------

qualifying_df = spark.read.schema(qualifying_schema).option("multiline", True).json("/mnt/formula082dl/raw/qualifying")

# COMMAND ----------

# MAGIC %md
# MAGIC #####Step2- Renamed columns and add new columns
# MAGIC 1.Renamed qualifyingld, driverld, constructorld and raceld
# MAGIC 2. Add ingestion_date with current timestamp

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

display(qualifying_df)

# COMMAND ----------

final_df = qualifying_df.withColumnRenamed("qualifyId", "qualify_id").withColumnRenamed("driverId", "driver_id").withColumnRenamed("raceId", "race_id").withColumnRenamed("constructorId", "constructor_id").withColumn("ingestion_date", lit(current_timestamp()))

# COMMAND ----------

# MAGIC %md
# MAGIC #####Step3- Write to output to processed container in parquet format

# COMMAND ----------

final_df.write.mode("overwrite").parquet("/mnt/formula082dl/processed/qualifying")  

# COMMAND ----------

display(spark.read.parquet("/mnt/formula082dl/processed/qualifying"))

# COMMAND ----------



# COMMAND ----------



# COMMAND ----------



# COMMAND ----------


