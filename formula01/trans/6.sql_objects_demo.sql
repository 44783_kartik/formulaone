-- Databricks notebook source
-- MAGIC %md
-- MAGIC ####Lesson Objectives
-- MAGIC 1.spark sql documnetation
-- MAGIC 2. create database demo
-- MAGIC 3.data tab in the UI
-- MAGIC 4.show command
-- MAGIC 5.DESCRIBE command
-- MAGIC 6.find the current database

-- COMMAND ----------

CREATE DATABASE demo;

-- COMMAND ----------

CREATE DATABASE IF NOT EXISTS demo;

-- COMMAND ----------

SHOW databases;

-- COMMAND ----------

DESCRIBE DATABASE demo;

-- COMMAND ----------

DESCRIBE DATABASE EXTENDED demo

-- COMMAND ----------

SELECT CURRENT_DATABASE()

-- COMMAND ----------

SHOW TABLES

-- COMMAND ----------

SHOW TABLES IN demo

-- COMMAND ----------

USE demo

-- COMMAND ----------

SELECT CURRENT_DATABASE();

-- COMMAND ----------

SHOW TABLES IN default;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####Learning Objectives
-- MAGIC 1.create managed table using python
-- MAGIC 2.create managed table using sql
-- MAGIC 3.effect of dropping a managed table
-- MAGIC 4.describe table

-- COMMAND ----------

-- MAGIC %run "../includes/configuration" 

-- COMMAND ----------

-- MAGIC %python
-- MAGIC race_results_df = spark.read.parquet(f"{presentation_folder_path}/race_results")

-- COMMAND ----------

-- MAGIC %python
-- MAGIC race_results_df.write.format("parquet").saveAsTable("demo.race_results_python")

-- COMMAND ----------

USE demo;
SHOW TABLES;

-- COMMAND ----------

DESC EXTENDED race_results_python;

-- COMMAND ----------

select * from demo.race_results_python
where race_year = 2020;

-- COMMAND ----------

CREATE TABLE demo.race_results_sql
AS
select * from demo.race_results_python
where race_year = 2020;

-- COMMAND ----------

SELECT CURRENT_DATABASE()

-- COMMAND ----------

DESC EXTENDED demo.race_results_SQL;  

-- COMMAND ----------

DROP TABLE demo.race_results_sql;

-- COMMAND ----------

SHOW TABLEs IN demo;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ##### learning objectives
-- MAGIC 1.Create external table using python
-- MAGIC 2.create external table using sql
-- MAGIC 3.effect of dropping an external table

-- COMMAND ----------

-- MAGIC %python
-- MAGIC  race_results_df.write.format("parquet").option("path",f"{presentation_folder_path}/race_results_ext_py").mode("overwrite")\
-- MAGIC      .saveAsTable("demo.race_results_ext_py")

-- COMMAND ----------

DESC EXTENDED demo.race_results_ext_py

-- COMMAND ----------

CREATE TABLE demo.race_results_ext_sql
(
race_year INT,
race_name STRING,
driver_name STRING,
driver_number INT,
driver_nationality STRING,
team STRING,
grid INT,
fastest_lap_time STRING,
race_time STRING,
points FLOAT,
position INT,
created_date TIMESTAMP

)
USING parquet
LOCATION "/mnt/formula082dl/presentation/race_results_ext_sql"

-- COMMAND ----------

drop table demo.race_results_ext_sql

-- COMMAND ----------

show tables in demo

-- COMMAND ----------

insert into demo.race_results_ext_sql
select * from demo.race_results_ext_py where race_year = 2020;

-- COMMAND ----------

select * from demo.race_results_ext_sql;

-- COMMAND ----------


